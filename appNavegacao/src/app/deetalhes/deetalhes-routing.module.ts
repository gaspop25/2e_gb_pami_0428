import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeetalhesPage } from './deetalhes.page';

const routes: Routes = [
  {
    path: '',
    component: DeetalhesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeetalhesPageRoutingModule {}
