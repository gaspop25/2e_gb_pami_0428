import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeetalhesPageRoutingModule } from './deetalhes-routing.module';

import { DeetalhesPage } from './deetalhes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeetalhesPageRoutingModule
  ],
  declarations: [DeetalhesPage]
})
export class DeetalhesPageModule {}
